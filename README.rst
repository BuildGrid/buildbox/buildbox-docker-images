What is this for ?
==================
This repository creates and publishes a docker image which is used
as a base OS to build the `buildbox static binaries <https://gitlab.com/BuildGrid/buildbox/buildbox-integration/>`__.



How do we maintain it ?
-----------------------
We shouldn't need to change this repository much if at all. If the
`buildbox static binaries <https://gitlab.com/BuildGrid/buildbox/buildbox-integration/>`__
need new system dependencies baked into the docker image, we can update the
Dockerfile in this repository.

Pushing changes in this repository will result in building and publishing a
new docker image.

It is necessary to *observe the CI job* in the gitlab UI, near the end of the
successful build you will see output which looks like the following:

.. code::

   Successfully built 2a472a92134a
   Successfully tagged registry.gitlab.com/buildbox/buildbox-docker-images/artifact-cache:master-609153162
   $ docker push $CI_REGISTRY_IMAGE/artifact-cache:$FIXED_TAG_SUFFIX

The new image will have a newly generated tag, in the example above it was ``master-609153162``.

Now that you have the new tag needed to index the image, you will have to update the
`buildbox-integration <https://gitlab.com/BuildGrid/buildbox/buildbox-integration/>`__
repository ``.gitlab-ci.yml`` accordingly, and assign this new tag value to the
``DEBIAN_IMAGE_TAG`` variable.

Then you will be able to use the new image to build the buildbox binaries.
